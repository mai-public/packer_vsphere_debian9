.DEFAULT_GOAL := help
.PHONY: help run python build shell env
SHELL := /bin/bash
MFDIR := $(abspath $(dir $(lastword $(MAKEFILE_LIST))))
help: ## Displays this help message
	@grep -E '^[a-zA-Z0-9_-]+?:[ a-zA-Z0-9_-]*?##.*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## *"}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

run: ## Runs packer build
	@cd $(MFDIR) && make python;
	
	@{ cd $(MFDIR) && make web_run && ((docker-compose run --rm packer) |& tee run.log ); } \
		|| make web_down
	@make -f Makefile web_down

python: ## builds only python template
	@cd $(MFDIR) && rm -f workspace/debian9.json && ./template.py
	@cd $(MFDIR) && rm -f preseeds/debian9.cfg   && ./preseeds/debian9.py

build: ## builds or rebuilds the image for packer
	@docker image rm -f mai/packer:1.0; true;
	@cd $(MFDIR) && docker build -t mai/packer:1.0 .

shell: ## Run the docker-compose service and enter the shell at /bin/sh
	@docker-compose run --rm packer /bin/sh
env: ## Creates env files in base dir from templates
	@cp env_templates/* .

web_run: ## Runs webpage that hosts static files
	@source $(MFDIR)/.make_env \
		&& cp $(MFDIR)/preseeds/debian9.cfg $${STATIC_FILES}/ \
		&& cd $${STATIC_COMPOSE_DIR} \
		&& docker-compose up -d \

web_down: ## takes down static files webpage
	@source $(MFDIR)/.make_env \
		&& rm -f $${STATIC_FILES}/debian9.cfg \
		&& cd $${STATIC_COMPOSE_DIR} \
		&& docker-compose down
