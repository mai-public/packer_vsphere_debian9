#!/usr/bin/env python3
from __future__ import print_function
import os, json
preseed_dir = os.path.dirname(os.path.realpath(__file__))
export_file = os.path.sep.join([preseed_dir, "debian9.cfg"])
env_file = os.path.sep.join([preseed_dir, "..", ".python_env"])

with open(env_file, 'r') as f:
    secrets = json.load(f)
vm_username = secrets['vm_username']
vm_password = secrets['vm_password']

lvm_scheme = r"""
d-i partman-auto/expert_recipe string \
        scheme ::                     \
        200 0 200 ext4                \
                $primary{ }           \
                $bootable{ }          \
                method{ format }      \
                format{ }             \
                use_filesystem{ }     \
                filesystem{ ext4 }    \
                mountpoint{ /boot } . \
        150% 0 150% linux-swap        \
                $primary{ }           \
                method{ swap }        \
                format{ } .           \
        1 0 -1 ext4                   \
                $primary{ }           \
                method{ format }      \
                format{ }             \
                use_filesystem{ }     \
                filesystem{ ext4 }    \
                mountpoint{ / } .


"""
preseed = r"""
d-i debian-installer/locale string en_US
d-i keyboard-configuration/xkb-keymap select us

# Static network configuration.
#
# IPv4 example
d-i netcfg/get_ipaddress string 192.168.0.77
d-i netcfg/get_netmask string 255.255.255.0
d-i netcfg/get_gateway string 192.168.0.1
d-i netcfg/get_nameservers string 10.10.10.6,1.1.1.1
d-i netcfg/confirm_static boolean true

# Any hostname and domain names assigned from dhcp take precedence over
# values set here. However, setting the values still prevents the questions
# from being shown, even if values come from dhcp.
d-i netcfg/get_hostname string unassigned-hostname
d-i netcfg/get_domain string unassigned-domain

# If you want to force a hostname, regardless of what either the DHCP
# server returns or what the reverse DNS entry for the IP is, uncomment
# and adjust the following line.
d-i netcfg/hostname string mai-vm-debian-1

# Disable that annoying WEP key dialog.
d-i netcfg/wireless_wep string

d-i passwd/root-password-again password {password}
d-i passwd/root-password password {password}
d-i passwd/user-fullname string {username}
d-i passwd/username string {username}
d-i passwd/user-password password {password}
d-i passwd/user-password-again password {password}

d-i clock-setup/utc boolean true
d-i time/zone string UTC

#mirror and archives
d-i mirror/country string manual
d-i mirror/http/hostname string http.us.debian.org
d-i mirror/http/directory string /debian
d-i mirror/http/proxy string

# Participate in survey related to package usage?
popularity-contest popularity-contest/participate boolean false


# partitions

# If one of the disks that are going to be automatically partitioned
# contains an old LVM configuration, the user will normally receive a
# warning. This can be preseeded away...
d-i partman-lvm/device_remove_lvm boolean true
# The same applies to pre-existing software RAID array:
d-i partman-md/device_remove_md boolean true
# And the same goes for the confirmation to write the lvm partitions.
d-i partman-lvm/confirm boolean true
d-i partman-lvm/confirm_nooverwrite boolean true

d-i partman-auto/method string regular
{lvm_scheme}

d-i partman-md/confirm boolean true
d-i partman-partitioning/confirm_write_new_label boolean true
d-i partman/choose_partition select finish
d-i partman/confirm boolean true
d-i partman/confirm_nooverwrite boolean true

# say no to "do you wish to scan another CD?" question

d-i apt-setup/cdrom/set-first boolean false
# ?
d-i apt-setup/non-free boolean true
d-i apt-setup/contrib boolean true

tasksel tasksel/first multiselect
d-i pkgsel/include string curl openssh-server sudo open-vm-tools build-essential
# Allowed values: none, safe-upgrade, full-upgrade
d-i pkgsel/upgrade select none

d-i grub-installer/bootdev string default
d-i grub-installer/only_debian boolean true

d-i finish-install/reboot_in_progress note

d-i preseed/late_command string                                                   \
        echo 'Defaults:{username} !requiretty' > /target/etc/sudoers.d/{username};      \
        echo '{username} ALL=(ALL) NOPASSWD: ALL' >> /target/etc/sudoers.d/{username};  \
        chmod 440 /target/etc/sudoers.d/{username}
""".format(username=vm_username, password=vm_password, lvm_scheme=lvm_scheme)
with open(export_file, "w") as f:
    f.write(preseed)
