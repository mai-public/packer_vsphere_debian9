#!/usr/bin/env python3
from __future__ import print_function, absolute_import, with_statement, generators
import os
import json
"""
Purpose: Takes variables from json file and fills out packer templates
"""
main_dir = os.path.dirname(os.path.realpath(__file__))
export_file = os.path.sep.join([main_dir, "workspace", "debian9.json"])
env_file = os.path.sep.join([main_dir, ".python_env"])

with open(env_file, 'r') as f:
    secrets = json.load(f)

username       = secrets['username']
password       = secrets['password']
vsphere_host   = secrets['vsphere_host']
host           = secrets['host']

vm_name        = secrets['vm_name']
guest_type     = secrets['guest_type']
cpu            = int(secrets['cpu'])
ram            = int(secrets['ram'])
network        = secrets['network']
datastore      = secrets['datastore']
iso_datastore  = secrets['iso_datastore']
iso_path       = secrets['iso_path']
disk_size      = int(secrets['disk_size'])
preseed_url    = secrets['preseed_url']
vm_username    = secrets['vm_username']
vm_password    = secrets['vm_password']
domain         = secrets['domain']
make_template  = secrets['make_template']

builder = {

"type": "vsphere-iso",

      "vcenter_server":      vsphere_host,
      "username":            username,
      "password":            password,
      "insecure_connection": "true",

      "vm_name": vm_name,
      "host":     host,


      "datastore": datastore,
      "network": network,

      "guest_os_type": guest_type,

      "ssh_username": vm_password,
      "ssh_password": vm_username,

      "CPUs":             1,
      "RAM":              2048,
      "RAM_reserve_all": True,
      "CPU_hot_plug": True,
      "RAM_hot_plug": True,
      "disk_controller_type":  "pvscsi",
      "disk_size":        disk_size,
      "disk_thin_provisioned": True,

      "network_card": "vmxnet3",
      "convert_to_template": make_template,
      "iso_paths": [
        "[{}] {}".format(iso_datastore, iso_path)
      ],
      "boot_command": [
          "<esc><wait>",
          "install ",
          "auto=true ",
          "interface=auto ",
          "url={} ".format(preseed_url),
          "hostname={} ".format(vm_name),
          "domain={} ".format(domain),
          "<enter>"

      ]
    }
provisioners = [
    {
      "type": "shell",
      "inline": ["echo '{vm_password}' | sudo apt install -y git parted dnsutils".format(vm_password=vm_password)]
    }
  ]

json_main = {
  "builders": [
    builder
  ],

  "provisioners": provisioners
}

with open(export_file, 'w') as f:
    json.dump(json_main, f)
    
