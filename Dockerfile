FROM alpine:3.9.4

RUN cd /tmp && \
   wget --no-cache https://releases.hashicorp.com/packer/1.4.2/packer_1.4.2_linux_amd64.zip https://github.com/jetbrains-infra/packer-builder-vsphere/releases/download/v2.3/packer-builder-vsphere-iso.linux \
   && unzip packer_1.4.2_linux_amd64.zip \
   && chmod +x packer-builder-vsphere-iso.linux \
   && mv packer-builder-vsphere-iso.linux packer /bin/ \
   && rm /tmp/* \
   && mkdir /sandbox

WORKDIR /sandbox


